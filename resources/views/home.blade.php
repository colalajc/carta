<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LA CARTA</title>

    <link rel="stylesheet" href="{{asset('mycss/mycss.css')}}"/>
    <link rel="stylesheet" href="{{asset('myscss/myscss.scss')}}"/>

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

</head>
<body>

    <section id="section_1">

        <span>H</span><span>O</span><span>L</span><span>A</span><span>&nbsp;</span><span>H</span><span>E</span><span>R</span><span>M</span><span>O</span><span>S</span><span>A</span>
    </section>
    <section id="section_1_1" class="d_n">
        <span>E</span><span>S</span><span>P</span><span>E</span><span>R</span><span>O</span><span>&nbsp;</span><span>T</span><span>E</span><span>&nbsp;</span><span>G</span><span>U</span><span>S</span><span>T</span><span>E...</span>
    </section>

    <section id="section_2" class="d_n">

        <div class="section_2_div_1">

            <span>C</span><span>O</span><span>N</span><span>O</span><span>C</span><span>E</span><span>R</span><span>T</span><span>E</span><span>&nbsp;</span><span>F</span><span>U</span><span>E</span><span>&nbsp;</span><span>L</span><span>O</span><span>&nbsp;</span><span>M</span><span>Á</span><span>S</span>
        </div>

        <div class="section_2_div_2">

            <span>H</span><span>E</span><span>R</span><span>M</span><span>O</span><span>S</span><span>O</span>
        </div>

        <div class="section_2_div_3">

            <span>Q</span><span>U</span><span>E</span><span>&nbsp;</span><span>M</span><span>E</span><span>&nbsp;</span><span>P</span><span>A</span><span>S</span><span>O</span>&nbsp;<span>B</span><span>A</span><span>B</span><span>Y...</span>
        </div>
    </section>
    <section id="gif" class="d_n" >
        <img src="{{asset('git/encuentro_2.gif')}}">
    </section>

    <section id="conteo" class="d_n" >
        <img src="{{asset('git/conteo.gif')}}">
    </section>

     <section id="section_3" class="d_n">
        <span>A</span><span>B</span><span>R</span><span>I</span><span>E</span><span>N</span><span>D</span><span>O</span><span>&nbsp;</span><span>C</span><span>A</span><span>R</span><span>T</span><span>A...</span>
    </section>

    <section id="carta">
        <div id="div_carta_1" class="d_n" >
            <label> - QUE TE AGRADEZCO </label>
            <p> Te agradezco por llegar a mi vida, por hacerme tan feliz, por siempre estar aquí conmigo, siempre con ganas de intentar y que todo esto funcione y sobre todo gracias por aceptarme en tu vida... </p>
        </div>

        <div id="div_carta_2" class="d_n" >
            <label> - QUE ME GUSTA DE TI </label>
            <p> Literalmente me gusta todo de ti, desde lo hermosa que eres y lo sexy... hasta como eres, como te puedes expresar con tanta fluidez, como piensas me fascina, no siempre estamos de acuerdo, pero siempre te escucho y aprendo algo y asu vez te comprendo. Me gusta tu iniciativa, de repente si tú no me decías para vernos, quisas todavía seriamos unos completos desconocidos :D , me gusta lo dedicada que eres en todo lo que haces...</p>
        </div>

        <div id="div_carta_3" class="d_n" >
            <label> - QUE ME GUSTA DE TI </label>
            <p> Tu amor siempre me tienes una anécdota que contar esas anécdotas locas que me sacan una carcajada y te lo agradezco mucho esas cosas me dan mucha paz y también están esos días malos como el día que me enferme y fue lindo que me cuidaras, me sentía bien, bueno no tanto... pero me sentía bien porque tú estabas acá y sobre todo me gusta lo madura que eres y esa madures hace que nuestra relación sea muy estable y muy bonita, no cambies mi vida... </p>
        </div>

        <div id="div_carta_4" class="d_n" >
            <label> - QUE ME GUSTA DE NUESTRA RELACIÓN </label>
            <p> A mí me gusta como comenzó y de como ese día en el parque nos dijimos lo que buscábamos lo que queríamos el uno del otro, me gusta que tengamos planes de casarnos de formar una familia, de viajar de salir a conocer otros lugares y lo vamos a lograr amor, viajar es una de las cosas que más me gusta hacer y es lindo que la persona con la cual estas te diga vamos y nos vamos...</p>
        </div>

        <div id="div_carta_5" class="d_n" >
            <label> - QUE ME GUSTA DE NUESTRA RELACIÓN </label>
            <p>Sé que vamos a pelear a resentirnos y todo lo demás, pero sé que lo vamos a poder resolver, porque así como empezó sé que hablando lo podemos resolver y sabes algo amor yo confió mucho en voz, eso me gusta de la relación que hay confianza y eso hermosa es la base de una relación, más allá de la atracción que tenemos confiar en alguien no es fácil, pero yo confió en ti y siempre lo haré y quiero seguir con nuestra relación por el resto de nuestras vidas...</p>
        </div>

        <div id="div_carta_6" class="d_n" >
            <label> QUE COSAS ME GUSTARIA AGREGAR </label>
            <p>Claro que mi baile sexy es una de esas cosas jajjaaj, más viajes amor ... muchos.... Quisiera conocer más a tus amigos y a tu familia, también podíamos experimentar nuevas cosas en lo íntimo :D , creo que cada día que pasamos agregamos algo más a la relación la cual se fortalece más y más... </p>
        </div>

        <div id="div_carta_7" class="d_n" >
            <label> TE PERDONO POR  </label>
            <p> Perdonarte amor .... mmmm ..... te perdono por la comparativa, te perdono por las cosas que vendrán jajajaja .... no hay mucho que perdonar amor  </p>
        </div>

        <div id="div_carta_8" class="d_n" >
            <label> TE PIDO PERDON POR  </label>
            <p>Hay varias cosas que quiero disculparme, perdón por a veces no saber como expresar esto que siento, es algo tan lindo que en mi mente solo está una cosa y la cual es que no quiero perderte, y si algún momento te sentiste insegura de lo que siento, te pido perdón amor, también porque a veces no tengo tiempo, por no hacerte llegar a veces :( , te pido perdón por todo.. </p>
        </div>

        <div id="div_carta_9" class="d_n" >
            <label> TE PIDO PERDON POR  </label>
            <p> Prometo mejorar y siempre escucharte y entender como te sientes para no volver a hacerlo y sobre todo perdón por a veces estar un poquito distraído así como ese día, mejoraré amor ya verás...</p>
        </div>

    </section>

    <section id="section_4" class="d_n">
       <div onclick="next_atras('atras')"> ATRAS</div>
       <div onclick="next_atras('delante')">SIGUIENTE</div>
    </section>

    <section id="section_5" class="d_n">
        <span>T</span><span>E</span></span>&nbsp;<span><span>A</span><span>M</span><span>O</span>&nbsp;<span><span>A</span><span>M</span><span>O</span><span>R</span>
    </section>

    <section id="te_amo" class="d_n" >
        <img src="{{asset('git/te_amo.gif')}}">
    </section>
</body>

<body>
    <audio width="320" height="176" controls autoplay loop>
        <source src="{{asset('mp3/musica_amor.mp3')}}"  type="audio/mp3">
    </audio>
    @include('_myjs')
</body>
</html>