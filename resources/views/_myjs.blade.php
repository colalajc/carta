<script type="text/javascript">

  $(document).ready(function() {
    
    //Ejecutamos la función para que evalue los condicionales cada 5 segundos
    setInterval(hideShowContainers, 5000);
    setInterval(hideShowContainers_2, 15100);
    setInterval(hideShowContainers_3, 22000);
    setInterval(hideShowContainers_4, 27000);
    setInterval(hideShowContainers_5, 32500);
    setInterval(hideShowContainers_6, 41000);
  });

  function hideShowContainers() {
    $('#section_1_1').show(3000);
    
  }
  function hideShowContainers_2() {
    $('#section_2').show(3000);
    
  }
  function hideShowContainers_3() {
    $('#gif').show();
    
  }

  function hideShowContainers_4() {
    $('#conteo').show();
    
  }

  function hideShowContainers_5() {
    $('#section_3').show(3000);
    
  }
  var entro=0;
  function hideShowContainers_6() {
    if(entro==0){
      $('#div_carta_1').show(3000);
      $('#section_4').show(3000);
      entro=1;
    }
    
  }

  var max         = 9;
  var donde_estoy = 1;

  const next_atras=(op)=>{
    var casos     = 0;
    var anterior  = 0;
    if(donde_estoy==1 && op=='atras'){
      donde_estoy=2;
    }
    if(donde_estoy==max && op=='delante'){
      donde_estoy=0;
      casos=max;
      $('#div_carta_'+max).hide();
      $('#section_4').hide();
      $('#te_amo').show();
      $('#section_5').show(3000);
      return false;

    }

    if (op=='atras') {
      anterior=donde_estoy + casos;
      donde_estoy= donde_estoy -1 ;

    }else{
      anterior=donde_estoy + casos;
      donde_estoy= donde_estoy +1 ;

    }
    $('#div_carta_'+anterior).hide();
    $('#div_carta_'+donde_estoy).show(3000);

  }
</script>